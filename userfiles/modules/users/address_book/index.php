<?php $user = get_user_by_id(user_id());
$countries_all = mw()->forms_manager->countries_list();
?>
<script src="admin/sweetalert/sweetalert.min.js"></script>
<style>
    .dt-address-form-content {
        padding: 15px;
        box-shadow: 0 0 6px 1px #ccc;
        border-radius: 5px;
        margin: 5px 5px;
    }

    .dt-address-form-content .form-group {
        margin-bottom: 8px !important;
    }

    .dt-address-form-content .dt-address-form-heading {
        border-bottom: 1px solid #eee;
        margin-bottom: 5px;
    }

    .dt-address-form-content .dt-address-form-heading h3 {
        font-size: 22px;
    }

    .dt-address-form label {
        font-weight: 600;
        font-size: 14px;
        margin-bottom: 2px !important;
    }

    .dt-address-form label>span {
        color: red;
    }

    .dt-addreess-from-modal .modal-dialog {
        max-width: 800px !important;
    }

    .dt-addreess-from-modal.table-modal .modal-dialog {
        max-width: 900px !important;

    }

    /* Table */
    .dt-address-form-modal-head .form-modal-icon i {
        font-weight: 300;
    }

    /* RESPONSIVE */
    @media (max-width: 445px) {
        .dt-address-form-content {
            margin: 10px 0px;
        }

        .dt-address-form .dt-address-form-heading h3 {
            font-size: 20px;
        }
    }


    .billingAddressCheckbox {
        display: none;
    }

    .billingAddressCheckbox.show {
        display: block;
    }

    /* Address book card */
    .address-book-wrapper {
        padding: 0px;
        padding-bottom: 30px;
    }

    .address-book-wrapper .breadcrumb {
        background-color: transparent;
        padding: 0;
    }

    .address-book-wrapper .breadcrumb-item+.breadcrumb-item::before {
        content: ">";
        font-weight: 600;
    }

    .address-book-wrapper .col {
        padding-left: 5px !important;
        padding-right: 5px;
        margin-bottom: 10px;
        display: none;
    }

    .address-book-card {
        border: 2px solid #999;
        border-radius: 10px;
        min-height: 100%;
        padding: 20px 10px;
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: space-between;

    }

    .address-book-card.add-card {
        display: flex;
        justify-content: center;
        align-items: center;
        border: 2px dotted #999;
        /* min-height: 300px; */
        border-radius: 10px;
    }

    .address-book-card.address-details-card .card-content {
        height: 100%;
        margin-bottom: 10px;
    }

    .address-book-card.address-details-card .address-details {
        max-width: 80%;
        margin-bottom: 0px;
        margin-top: 5px;
    }

    .address-book-card.add-card .add-icon {
        display: flex;
        justify-content: center;
        margin-bottom: 10px;
        color: #999;
    }

    .address-book-card .address-book-actions {
        /* position: absolute;
        bottom: 10px;
        left: 10px; */
    }

    .address-book-card .address-book-actions .btn {
        padding: 0;
        border-radius: 0;
        margin-right: 5px;
        padding-right: 5px;
        text-decoration: none;
        position: relative;
        font-size: 13px;
    }

    .address-book-card .address-book-actions .btn:not(:last-child):after {
        content: "";
        width: 2px;
        height: 15px;
        background-color: #333;
        position: absolute;
        right: -3px;
        top: 50%;
        transform: translateY(-50%);
    }
</style>

<?php $addresses = DB::table('users_addresses')->where('user_id', $user['id'])->get();
?>

<!-- Address Management MODAL -->
<div class=" dt-addreess-from-modal table-modal" id="dtAddressFormModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="address-book-wrapper">
                    <h2 class="mb-3 mt-4"><?php _e('My Address Book') ?></h2>
                    <!-- Card -->
                    <div class="row">
                        <div class="col-4 col">
                            <div class="address-book-card add-card" data-toggle="modal" data-target="#dtAddressAddModal" style="cursor: pointer;">
                                <div class="card-content">
                                    <span class="add-icon">
                                        <i class="fas fa-3x fa-plus"></i>
                                    </span>
                                    <h3><?php _e('Add Address') ?></h3>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($addresses as $address) :
                        ?>
                            <div class="col-4 col">
                                <div class="address-book-card address-details-card">
                                    <div class="card-content">
                                        <h4><?php print($address->address_title); ?></h4>
                                        <!-- <a href="#">Map</a> -->
                                        <address class="address-details">
                                            <h6><?php _e('Shipping Address') ?></h6>
                                            <?php print($address->shipping_name); ?>,
                                            <?php print($address->shipping_city); ?>,
                                            <?php print($address->shipping_zip); ?>,
                                            <?php print($address->shipping_state); ?>
                                        </address>

                                        <address class="address-details">
                                            <h6><?php _e('Billing Address') ?></h6>
                                            <?php print($address->billing_name); ?>,
                                            <?php print($address->billing_city); ?>,
                                            <?php print($address->billing_zip); ?>,
                                            <?php print($address->billing_state); ?>
                                        </address>
                                    </div>
                                    <div class="address-book-actions" id="address_book_type">
                                        <button class="btn btn-link" value="<?= $address->id ?>" onclick="addressEditView(this.value)"><?php _e('Edit') ?></button>
                                        <button class="btn btn-link" value="<?= $address->id ?>" onclick="addressDelete(this.value)"><?php _e('Delete') ?></button>
                                        <?php if (isset($address->address_type) && $address->address_type == 'Default') : ?>
                                            <?php print($address->address_type); ?>
                                        <?php else : ?>
                                            <button class="btn btn-link" value="<?= $address->id ?>" onclick="defaultAddress(this.value)"><?php _e('Default Address') ?></button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                        <button class="btn btn-info load-more">Load More</button>
                        <button class="btn btn-info load-less" style="display: none;">Load Less</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Address Management Edit Modal -->
<div class="modal fade dt-addreess-from-modal" id="dtAddressEditModal" tabindex="-1" role="dialog" aria-labelledby="dtAddressEditModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="dtAddressEditModalClose" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="dt-address-form" action="<?php echo api_url('update_user_address') ?>">
                <div class="modal-body">
                    <div class="dt-address-form-content">
                        <div class="dt-address-form-heading d-flex align-items-center justify-content-between">
                            <h3><?php _e('Address title') ?> <span style='color: red; font-weight: 600' class="required-star ml-1">*</span></h3>
                            <div class="d-flex" id="checkbox">
                                <input class="form-check-input" name="address_type" type="checkbox" id="Default" value="Default">
                                <label class="form-check-label" for="inlineCheckbox1"><?php _e('Default Address') ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input id="edit_address_title" name="address_title" type="text" class="form-control" placeholder="Address Title" value="" onkeyup="editaddressTitle(this.value)" required />
                        </div>
                    </div>

                    <div class="dt-address-form-content mt-3">
                        <div class="dt-address-form-heading">
                            <h3><?php _e('Shipping Address') ?></h3>
                        </div>
                        <div class="form-group">
                            <label for="country">
                                Land auswählen
                                <span class="required-star">*</span>
                            </label>
                            <select id="address_edit_shipping_country" class="form-control" name="shipping_country">
                                <option value=""></option>
                                <?php foreach ($countries_all  as $item) : ?>
                                    <option value="<?php print $item ?>"><?php print $item ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="shipping_name"><?php _e('Name') ?> <span class="required-star">*</span></label>
                            <input id="address_edit_shipping_name" name="shipping_name" type="text" class="form-control" required />
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="zip">Postleitzahl
                                    <span class="required-star">*</span>
                                </label>
                                <input id="address_edit_shipping_zip" name="shipping_zip" type="text" class="form-control" value="" required />
                            </div>
                            <div class="col">
                                <label for="city">Stadt
                                    <span class="required-star">*</span>
                                </label>
                                <input id="address_edit_shipping_city" name="shipping_city" type="text" class="form-control" value="" required />
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label for="address">Straße und Hausnummer
                                <span class="required-star">*</span>
                            </label>
                            <input id="address_edit_shipping_state" name="shipping_state" type="text" class="form-control" value="" required />
                        </div>
                        <input id="address_edit_id" name="id" type="hidden" value="" />
                    </div>
                    <div class="dt-address-form-content mt-3">
                        <div class="dt-address-form-heading">
                            <h3><?php _e('Billing Address') ?></h3>
                        </div>
                        <div class="form-group">
                            <label for="country">
                                Land auswählen
                                <span class="required-star">*</span>
                            </label>
                            <select id="address_edit_billing_country" class="form-control" name="billing_country">
                                <option value=""></option>
                                <?php foreach ($countries_all  as $item) : ?>
                                    <option value="<?php print $item ?>"><?php print $item ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="shipping_name"><?php _e('Name') ?>  <span class="required-star">*</span></label>
                            <input id="address_edit_billing_name" name="billing_name" type="text" class="form-control" required />
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="zip">Postleitzahl
                                    <span class="required-star">*</span>
                                </label>
                                <input id="address_edit_billing_zip" name="billing_zip" type="text" class="form-control" value="" required />
                            </div>
                            <div class="col">
                                <label for="city">Stadt
                                    <span class="required-star">*</span>
                                </label>
                                <input id="address_edit_billing_city" name="billing_city" type="text" class="form-control" value="" required />
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label for="address">Straße und Hausnummer
                                <span class="required-star">*</span>
                            </label>
                            <input id="address_edit_billing_state" name="billing_state" type="text" class="form-control" value="" required />
                        </div>
                        <input name="user_id" type="hidden" value="<?php if (isset($user['id'])) print $user['id']; ?>" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Address Management Add Modal -->
<div class="modal fade dt-addreess-from-modal" id="dtAddressAddModal" tabindex="-1" role="dialog" aria-labelledby="dtAddressAddModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="dt-address-form" method="POST" action="<?php echo api_url('add_user_address') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="dtAddressAddModalClose" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="dt-address-form-content">
                        <div class="dt-address-form-heading d-flex align-items-center justify-content-between">
                            <h3><?php _e('Address title') ?> <span style='color: red; font-weight: 600' class="required-star ml-1">*</span></h3>
                            <div class="d-flex">
                                <input class="form-check-input" name="address_type" type="checkbox" id="Default" value="Default">
                                <label class="form-check-label" for="inlineCheckbox1"><?php _e('Default Address') ?> </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input id="add_address_title" name="address_title" type="text" class="form-control" placeholder="Adresstitel" value="" onkeyup="addressTitle(this.value)" required />
                        </div>
                    </div>

                    <!-- Shipping Address -->
                    <div class="dt-address-form-content mt-3">
                        <div class="dt-address-form-heading">
                            <h3><?php _e('Shipping Address') ?></h3>
                        </div>
                        <div class="form-group">
                            <label for="country">
                                Land auswählen
                                <span class="required-star">*</span>
                            </label>
                            <select class="form-control" name="shipping_country">
                                <option value=""><?php _e("Choose country"); ?></option>
                                <?php foreach ($countries_all  as $item) : ?>
                                    <option value="<?php print $item ?>" <?php if (isset($user['country']) && $user['country'] == $item || $item == "Germany") :; ?> selected="selected" <?php endif; ?>><?php print $item ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="shipping_name"><?php _e('Name') ?> <span class="required-star">*</span></label>
                            <input name="shipping_name" type="text" class="form-control" id="address_book_shipping_name" value="" onkeyup="shipping_name(this.value)" required />
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="zip">Postleitzahl
                                    <span class="required-star">*</span>
                                </label>
                                <input name="shipping_zip" type="text" class="form-control" id="shipping_zip" value="" onkeyup="shipping_zip(this.value)" required />
                            </div>
                            <div class="col">
                                <label for="city">Stadt
                                    <span class="required-star">*</span>
                                </label>
                                <input name="shipping_city" type="text" class="form-control" id="shipping_city" value="" onkeyup="shipping_city(this.value)" required />
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label for="address">Straße und Hausnummer
                                <span class="required-star">*</span>
                            </label>
                            <input name="shipping_state" type="text" class="form-control" id="shipping_address" value="" onkeyup="shipping_address(this.value)" required />
                        </div>
                        <input name="user_id" type="hidden" value="<?php if (isset($user['id'])) print $user['id']; ?>" />
                    </div>

                    <!-- As default form -->
                    <div class="text-right mt-3">
                        <input class="blling-address-checkbox" id="blling-address-checkbox" name="billing_type" type="checkbox" value="same_shipping" checked>
                        <label for="#"><?php _e('The billing and delivery addresses are the same') ?>.</label>
                    </div>
                    <!-- Billing Address -->
                    <div class="billingAddressCheckbox">
                        <div class="dt-address-form-content mt-3">
                            <div class="dt-address-form-heading">
                                <h3><?php _e('Billing Address') ?></h3>
                            </div>
                            <div class="form-group">
                                <label for="country">
                                    Land auswählen
                                    <span class="required-star">*</span>
                                </label>
                                <select class="form-control" name="billing_country">
                                    <option value=""><?php _e("Choose country"); ?></option>
                                    <?php foreach ($countries_all  as $item) : ?>
                                        <option value="<?php print $item ?>" <?php if (isset($user['country']) && $user['country'] == $item || $item == "Germany") :; ?> selected="selected" <?php endif; ?>><?php print $item ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="shipping_name"><?php _e('Name') ?>  <span class="required-star">*</span></label>
                                <input name="billing_name" type="text" class="form-control" id="address_book_billing_name" value="" required />
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <label for="zip">Postleitzahl
                                        <span class="required-star">*</span>
                                    </label>
                                    <input name="billing_zip" type="text" class="form-control" id="address_book_billing_zip" value="" required />
                                </div>
                                <div class="col">
                                    <label for="city">Stadt
                                        <span class="required-star">*</span>
                                    </label>
                                    <input name="billing_city" type="text" class="form-control" id="address_book_billing_city" value="" required />
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label for="address">Straße und Hausnummer
                                    <span class="required-star">*</span>
                                </label>
                                <input name="billing_state" type="text" class="form-control" id="address_book_billing_address" value="" required />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function addressView(id) {
        var id = id;
        var user_id = <?php print user_id(); ?>;

        $.ajax({
            type: "POST",
            url: "<?= api_url('view_user_address') ?>",
            data: {
                id: id,
                user_id: user_id

            },
            success: function(data) {
                console.log(data);
                $('#view_address_title').val(data.address_response.address_title);
                $('#address_view_shipping_country').val(data.address_response.shipping_country);
                $('#address_view_shipping_name').val(data.address_response.shipping_name);
                $('#address_view_shipping_zip').val(data.address_response.shipping_zip);
                $('#address_view_shipping_city').val(data.address_response.shipping_city);
                $('#address_view_shipping_state').val(data.address_response.shipping_state);
                $('#address_view_billing_country').val(data.address_response.billing_country);
                $('#address_view_billing_name').val(data.address_response.billing_name);
                $('#address_view_billing_zip').val(data.address_response.billing_zip);
                $('#address_view_billing_city').val(data.address_response.billing_city);
                $('#address_view_billing_state').val(data.address_response.billing_state);
                $('#dtAddressViewModal').modal('show');
            }

        });
    }

    function addressDelete(id) {
        var id = id;
        swal({
            title: "<?php _e('Are you sure') ?>",
            text: "<?php _e('Once deleted, you will not be able to recover this Address') ?>!",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "<?= api_url('delete_user_address') ?>",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        swal("<?php _e('Your Address has been deleted') ?>.", {
                            icon: "success",
                        });
                        location.reload();
                    }
                });
            }
        });
    }


    function addressTitle(address_title) {
        var address_title = address_title;
        var user_id = <?php print user_id(); ?>;
            $.ajax({
                type: "POST",
                url: "<?= api_url('view_address_title') ?>",
                data: {
                    address_title: address_title,
                    user_id: user_id

                },
                success: function(data) {

                    if (data.address_response == 'true') {
                        // document.getElementById("add_address_title").value = "";
                        document.getElementById("add_address_title").placeholder = "Dieser Adresstitel existiert bereits.";
                        document.getElementById("add_address_title").style.background = "#FADBD8 ";
                    } else {
                        document.getElementById("add_address_title").style.background = "#ABEBC6";
                        document.getElementById("add_address_title").placeholder = "Bitte geben Sie den Adresstitel ein.";

                    }
                }
            });

    }

    function editaddressTitle(address_title) {

        var address_title = address_title;
        var user_id = <?php print user_id(); ?>;
            $.ajax({
                type: "POST",
                url: "<?= api_url('view_address_title') ?>",
                data: {
                    address_title: address_title,
                    user_id: user_id
                },
                success: function(data) {

                    if (data.address_response == 'true') {
                        // document.getElementById("edit_address_title").value = "";
                        document.getElementById("edit_address_title").placeholder = "Dieser Adresstitel existiert bereits.";
                        document.getElementById("edit_address_title").style.background = "#FADBD8 ";
                    } else {
                        document.getElementById("edit_address_title").style.background = "#ABEBC6";
                        document.getElementById("edit_address_title").placeholder = "Bitte geben Sie den Adresstitel ein.";

                    }
                }
            });

    }

    function addressEditView(id) {
        var id = id;
        $.ajax({
            type: "POST",
            url: "<?= api_url('view_user_address') ?>",
            data: {
                id: id
            },
            success: function(data) {
                console.log(data.checked);
                $('#edit_address_title').val(data.address_response.address_title);
                $('#address_edit_shipping_country').val(data.address_response.shipping_country);
                $('#address_edit_shipping_name').val(data.address_response.shipping_name);
                $('#address_edit_shipping_zip').val(data.address_response.shipping_zip);
                $('#address_edit_shipping_city').val(data.address_response.shipping_city);
                $('#address_edit_shipping_state').val(data.address_response.shipping_state);
                $('#address_edit_billing_country').val(data.address_response.billing_country);
                $('#address_edit_billing_name').val(data.address_response.billing_name);
                $('#address_edit_billing_zip').val(data.address_response.billing_zip);
                $('#address_edit_billing_city').val(data.address_response.billing_city);
                $('#address_edit_billing_state').val(data.address_response.billing_state);
                $('#address_edit_id').val(data.address_response.id);
                $("#checkbox").append(data.checked);
                $('#dtAddressEditModal').modal('show');
            }

        });
    }

    $('#address_book_shipping_name').keyup(function() {
        $('#address_book_billing_name').val($(this).val());
    });
    $('#shipping_city').keyup(function() {
        $('#address_book_billing_city').val($(this).val());
    });
    $('#shipping_zip').keyup(function() {
        $('#address_book_billing_zip').val($(this).val());
    });
    $('#shipping_address').keyup(function() {
        $('#address_book_billing_address').val($(this).val());
    });


    $(document).ready(function() {
        $("#blling-address-checkbox").change(function() {
            if ($(this).is(':checked')) {
                $(".billingAddressCheckbox").toggleClass('show');
                document.getElementById("address_book_billing_name").value = "";
                document.getElementById("address_book_billing_zip").value = "";
                document.getElementById("address_book_billing_city").value = "";
                document.getElementById("address_book_billing_address").value = "";
                document.getElementById("address_book_shipping_name").value = "";
                document.getElementById("shipping_zip").value = "";
                document.getElementById("shipping_city").value = "";
                document.getElementById("shipping_address").value = "";
            } else {
                $(".billingAddressCheckbox").toggleClass('show');
                document.getElementById("address_book_billing_name").value = "";
                document.getElementById("address_book_billing_zip").value = "";
                document.getElementById("address_book_billing_city").value = "";
                document.getElementById("address_book_billing_address").value = "";


            }
        });
    })


    $("button[data-dismiss=dtAddressEditModalClose]").click(function() {
        $('#dtAddressEditModal').modal('hide');

    });


    $("button[data-dismiss=dtAddressViewModalClose]").click(function() {
        $('#dtAddressViewModal').modal('hide');

    });

    $("button[data-dismiss=dtAddressAddModalClose]").click(function() {
        $('#dtAddressAddModal').modal('hide');

    });

    function defaultAddress(id) {
        var id = id;
        var user_id = <?php print user_id(); ?>;
        swal({
            title: "<?php _e('Are you changing it') ?>?",
            text: "<?php _e('You change This Address as Default Address') ?> !",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "<?= api_url('update_address_type') ?>",
                    data: {
                        id: id,
                        user_id: user_id
                    },
                    success: function(data) {
                        swal("<?php _e('Your address is the Default Address') ?>.", {
                            icon: "success",
                        });
                        location.reload();
                    }
                });
            }
        });
    }

</script>

<script>
    $(document).ready(function(){
    $("#dtAddressFormModal .address-book-wrapper .col").slice(0, 3).show();
    $("#dtAddressFormModal .load-more").on("click", function(e){
      e.preventDefault();
      $("#dtAddressFormModal .address-book-wrapper .col:hidden").slice(0, 3).slideDown();
      if($("#dtAddressFormModal .address-book-wrapper .col:hidden").length == 0) {
        $("#dtAddressFormModal .load-more").css("display", "none");
        $("#dtAddressFormModal .load-less").css("display", "block");
      }
      $("#dtAddressFormModal .load-less").on("click", function(){
            $("#dtAddressFormModal .load-more").css("display", "block");
            $("#dtAddressFormModal .load-less").css("display", "none");
            $("#dtAddressFormModal .address-book-wrapper .col").hide();
            $("#dtAddressFormModal .address-book-wrapper .col").slice(0, 3).show();

        });
    });

  })
</script>
