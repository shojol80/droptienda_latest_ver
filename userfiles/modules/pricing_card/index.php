<link rel="stylesheet" type="text/css" href="<?php print $config['url_to_module'] ?>css/pricing_card.css" />

<style>
   .sort-icon-wrapper {
        position: relative;
        padding-left: 30px;
    }

    .mw-live-edit .sort-icon-wrapper .row-heading {
        margin-left: 30px;
    }

    .pricing-table-body .sort-icon {
        display: none;
    }

    .mw-live-edit .sort-icon {
        position: absolute;
        /* right: 150px; */
        left: 5px;
        top: 50%;
        transform: translateY(-50%);
        background-color: #0879fa;
        color: #fff;
        border-radius: 5px;
        width: 30px;
        height: 30px;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: move;
        opacity: 0;
        transition: opacity .2s ease;
    }

    .mw-live-edit .pricing-table-body tr:hover .sort-icon {
        opacity: 1 !important;
        display: flex;
    }

</style>

<?php $card_limit_quantity = get_option($params['id'],'pricing_card_limit_quantity'); ?>
<?php $table_limit_quantity = get_option($params['id'],'pricing_table_limit_quantity');   ?>
<?php
    $table_readmore_limit = get_option('table_readmore_'.$params['id'],'pricing_table_readmore_row_limit');
    if(!$table_readmore_limit){
        $table_readmore_limit = 'off';
    }
?>
<?php
    $interval_limit = get_option('interval_limit','pricing_interval_'.$params['id']);
?>
<?php $active_card_number = get_option($params['id'],'pricing_card_active');   ?>
<?php $interval_information = DB::table('options')->where('option_group','pricing_interval_info_'.$params['id'])->orderBy('option_key','asc')->get();
    echo pricingCardString($params,$interval_information,$card_limit_quantity,$active_card_number,$table_limit_quantity,$table_readmore_limit);
    ?>

<?php include modules_path() . "pricing_card/price_table.php"; ?>

<script>
$(".heading.edit.parent-heading").on("keyup",function(){
    let id = $(this).data("id");
    let field = $(".heading.edit").attr("field");
    let text = $(this).html();
    $('.child-heading-'+id).html(text);
});

$(".heading.edit.child-heading").on("keyup",function(){
    let id = $(this).data("id");
    let field = $(".heading.edit").attr("field");
    let text = $(this).html();
    $('.parent-heading-'+id).html(text);
});
</script>

<script>

$(function(){
    const isLiveEditOn = $("body").hasClass("mw-live-edit");
    if (isLiveEditOn) {
        $('.pricing-table-body .sort-icon ').css('display', 'flex')
        $('tbody.pricing-table-body').each(function(i){
        var sortedTBody = $(this).attr('id', 'pricing-table-body' + '-' + i);
        sortedTBody.sortable({
            revert: 100,
            axis: 'y',
            cursor: "move",
            cancel: ".pricing-table-heading-tr, .readmore-td",
            helper: function(e, row) {
                row.children().each(function() {
                    $(this).width($(this).width());
                });
                return row;
            },

            update: function (event, ui) {
                var orderdItems = sortedTBody.sortable('toArray', { attribute: 'data-serial' });

                $.ajax({
                    type: "POST",
                    url: "<?=api_url('save_price_table_position')?>",
                    data:{orderdItems},
                    success: function(response) {
                        console.log('success');
                        mw.notification.success('Saved successfully')
                    }
                });
            }
        })
    });
}
});


</script>
