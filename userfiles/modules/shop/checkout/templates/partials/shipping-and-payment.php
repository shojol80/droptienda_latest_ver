<style>
    .well{
        min-height:auto !important;
    }
</style>

<?php if(is_logged()) :?>
    <?php $user = get_user(); ?>
    <?php $addresses = DB::table('users_addresses')->where('user_id', $user['id'])->get(); ?>
    <?php endif; ?>


<div class="mw-ui-row shipping-and-payment mw-shop-checkout-personal-info-holder">
    <div class="mw-ui-col" style="width: 33%;">
        <div class="mw-ui-col-container">
            <?php if(is_logged()){ ?>
                <!-- Main -->
                <div class="well checkout-box-shadow-style" style="margin-bottom:30px">
                <!-- Is Address is empty -->
                    <?php  if ($addresses->isEmpty()) { ?>
                    <div class="d-flex align-items-center justify-content-between">
                    <h3 style="margin-top:0 " class="nodrop checkout-form-heading " rel_id="<?php print $params['id'] ?>">
                        Persönliche Informationen
                    </h3>
                    <div class="d-flex">
                    <?php if (is_live_edit()) : ?>
                    <a href="" data-toggle="modal" ><span class="edit" rel="module" field="addressook_icon"><i class='fas fa-2x fa-address-book' style="font-weight: 300;"></i></span>
                    </a>
                    <?php else : ?>
                        <a href="" data-toggle="modal" data-target="#addressbook"><i class='fas fa-2x fa-address-book' style="font-weight: 300;"></i>
                    </a>
                    <?php endif; ?>
                            </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Vorname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="first_name" class="field-full form-control input-required" id="first_name" type="text"
                                    value="<?php if (isset($user['first_name'])) {
                                        print $user['first_name'];
                                    }elseif (@mw()->user_manager->session_get("first_name") != null){
                                        print mw()->user_manager->session_get("first_name");
                                    }?>" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Nachname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="last_name" id="last_name" class="field-full form-control input-required" type="text"
                                    value="<?php if (isset($user['last_name'])) {
                                        print $user['last_name'];
                                    }elseif (@mw()->user_manager->session_get("last_name") != null){
                                        print mw()->user_manager->session_get("last_name");
                                    } ?>" required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            <?php _e("Email"); ?> <span style="color: red;">*</span>
                        </label>
                        <input name="email" id="email" class="field-full form-control input-required" type="text"
                               value="<?php if (isset($user['email'])) {
                                   print $user['email'];
                               }elseif (@mw()->user_manager->session_get("email") != null){
                                   print mw()->user_manager->session_get("email");
                               } ?>" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            <?php _e("Telefon"); ?>
                        </label>
                        <input name="phone" id="phone" class="field-full form-control" type="text"
                               value="<?php if (isset($user['phone'])) {
                                   print $user['phone'];
                               }elseif (@mw()->user_manager->session_get("phone") != null){
                                   print mw()->user_manager->session_get("phone");
                               } ?>"/>
                    </div>
                    <?php }else{?>
                        <!-- Is Not Address is empty -->
                        <div class="d-flex align-items-center justify-content-between">
                    <h3 style="margin-top:0 " class="nodrop checkout-form-heading " rel_id="<?php print $params['id'] ?>">
                        Persönliche Informationen
                    </h3>
                    <div class="d-flex">
                    <?php if (is_live_edit()) : ?>
                    <a href="" data-toggle="modal" ><span class="edit" rel="module" field="addressook_icon"><i class='fas fa-2x fa-address-book' style="font-weight: 300;"></i></span>
                    </a>
                    <?php else : ?>
                        <a href="" data-toggle="modal" data-target="#checkout_address_book"><i class='fas fa-2x fa-address-book' style="font-weight: 300;"></i>
                    </a>
                    <?php endif; ?>
                            </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Vorname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="first_name" class="field-full form-control input-required" id="first_name" type="text"
                                    value="<?php if (isset($user['first_name'])) {
                                        print $user['first_name'];
                                    }elseif (@mw()->user_manager->session_get("first_name") != null){
                                        print mw()->user_manager->session_get("first_name");
                                    }?>" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Nachname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="last_name" id="last_name" class="field-full form-control input-required" type="text"
                                    value="<?php if (isset($user['last_name'])) {
                                        print $user['last_name'];
                                    }elseif (@mw()->user_manager->session_get("last_name") != null){
                                        print mw()->user_manager->session_get("last_name");
                                    } ?>" required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            <?php _e("Email"); ?> <span style="color: red;">*</span>
                        </label>
                        <input name="email" id="email" class="field-full form-control input-required" type="text"
                               value="<?php if (isset($user['email'])) {
                                   print $user['email'];
                               }elseif (@mw()->user_manager->session_get("email") != null){
                                   print mw()->user_manager->session_get("email");
                               } ?>" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            <?php _e("Telefon"); ?>
                        </label>
                        <input name="phone" id="phone" class="field-full form-control" type="text"
                               value="<?php if (isset($user['phone'])) {
                                   print $user['phone'];
                               }elseif (@mw()->user_manager->session_get("phone") != null){
                                   print mw()->user_manager->session_get("phone");
                               } ?>"/>
                    </div>
                        <?php } ?>
                </div>
            <?php }else{ ?>

                <div class="well checkout-box-shadow-style" style="margin-bottom:30px; display:none;" id="guest_check">
                    <?php $user = get_user(); ?>
                    <h3 style="margin-top:0 " class="nodrop checkout-form-heading" rel_id="<?php print $params['id'] ?>">
                        Persönliche Informationen

                    </h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Vorname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="first_name" class="field-full form-control input-required" id="first_name" type="text"
                                    value="<?php if (isset($user['first_name'])) {
                                        print $user['first_name'];
                                    }elseif (@mw()->user_manager->session_get("first_name") != null){
                                        print mw()->user_manager->session_get("first_name");
                                    }?>" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <?php _e("Nachname"); ?> <span style="color: red;">*</span>
                                </label>
                                <input name="last_name" id="last_name" class="field-full form-control input-required" type="text"
                                    value="<?php if (isset($user['last_name'])) {
                                        print $user['last_name'];
                                    }elseif (@mw()->user_manager->session_get("last_name") != null){
                                        print mw()->user_manager->session_get("last_name");
                                    } ?>" required/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label>
                            <?php _e("Email"); ?> <span style="color: red;">*</span>
                        </label>
                        <input name="email" id="email" class="field-full form-control input-required" type="text"
                               value="<?php if (isset($user['email'])) {
                                   print $user['email'];
                               }elseif (@mw()->user_manager->session_get("email") != null){
                                   print mw()->user_manager->session_get("email");
                               } ?>" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            <?php _e("Telefon"); ?>
                        </label>
                        <input name="phone" id="phone" class="field-full form-control" type="text"
                               value="<?php if (isset($user['phone'])) {
                                   print $user['phone'];
                               }elseif (@mw()->user_manager->session_get("phone") != null){
                                   print mw()->user_manager->session_get("phone");
                               } ?>"/>
                    </div>
                </div>

                <div class="well checkout-box-shadow-style" style="margin-bottom:30px;" id="hideit">
                    <p>Für die Kasse als Gast klicken Sie bitte auf die Schaltfläche</p>
                    <button type="button" class="btn btn-info" onclick="guest_check()">weiter als Gast</button>

                    <hr>


                    <p>oder Melden Sie sich mit Ihren Daten an</p>
                    <a href="#" class="js-login-modal btn btn-login btn-primary" data-toggle="modal" data-target="#loginModal">Anmeldung</a>

                </div>

            <?php } ?>
            <?php if ($cart_show_shipping != 'n'): ?>
                <div class="well mw-shop-checkout-shipping-holder checkout-box-shadow-style">

                    <module type="shop/shipping"/>

                </div>
            <?php endif; ?>
			<div class="mt-3" style="text-align:right;">
                <input type="checkbox" checked name="billing_checkbox" id="billing_checkbox">&nbsp;
                <label for="billing_checkbox">Lieferadresse entspricht der Rechnungsadresse</label>
            </div>
            <div class="billing_address mt-5" id="billing_address" style="display:none;">
                <div class="well mw-shop-checkout-shipping-holder checkout-box-shadow-style">

                    <module type="shop/billing"/>

                </div>
            </div>
            <?php if(is_logged()): ?>
            <!-- Shipping && billing -->
            <div class="modal " id="checkout_address_book" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
            <div class=" dt-addreess-from-modal table-modal">
        <div class="modal-dialog mt-4" style="margin-top: 100px !important; width: 900px !important;" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="address-book-wrapper" style="padding: 0px 10px !important;">
                    <h2 class="mb-3 mt-4">My Address Book</h2>
                    <!-- Card -->
                    <div class="row" >
                        <?php foreach ($addresses as $address) :
                        ?>
                            <div class="col-4 col" >
                                <div class="address-book-card address-details-card">
                                    <div class="card-content">
                                        <h4><?php print($address->address_title); ?></h4>

                                        <!-- <a href="#">Map</a> -->
                                        <address class="address-details">
                                        <h6>Versandinformationen</h6>
                                            <?php print($address->shipping_name); ?>,
                                            <?php print($address->shipping_city); ?>,
                                            <?php print($address->shipping_zip); ?>,
                                            <?php print($address->shipping_state); ?>
                                        </address>

                                        <address class="address-details">
                                        <h6>Rechnungsadresse</h6>
                                            <?php print($address->billing_name); ?>,
                                            <?php print($address->billing_city); ?>,
                                            <?php print($address->billing_zip); ?>,
                                            <?php print($address->billing_state); ?>
                                        </address>
                                    </div>
                                    <div class="address-book-actions text-center w-100 mt-3" id="address_book_type">

                                        <?php if (isset($address->address_type) && $address->address_type == 'Default') : ?>
                                            <h6 class="text-left"><b><?php print($address->address_type); ?></b></h6>
                                        <?php else : ?>

                                           <button class="btn btn-link rounded" type="button" style="padding: 3px 5px;" value="<?= $address->id ?>" onclick="addressSelect(this.value)"><?php _e('Use Address')?></button>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                        <button class="btn load-more btn-info" type="button">Load More</button>
                        <button class="btn load-less btn-info" type="button" style="display: none;">Load Less</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            </div>
            <?php endif; ?>
        </div>

    </div>


</div>



<script>
    function guest_check(){
        $('#hideit').css('display','none');
        $('#guest_check').css('display','block');
    }

	$('#billing_checkbox').click (function(){
        if(document.getElementById('billing_checkbox').checked){
            $('#billing_address').css('display', 'none');
        } else{
            $('#billing_address').css('display', '');
        }
    });
    // $("#guest-login").hide();
    // $(".btn-login").on('click',function(){
    //     $("#guest-login").show();
    // });


</script>

<?php if(is_logged()): ?>
<script>
    function addressSelect(id) {
    var id = id;
        $.ajax({
            type: "POST",
            url: "<?= api_url('view_address') ?>",
            data: {
                id: id
            },
            success: function(data) {
                console.log(data.address_response);
                $('#checkout_address_book').modal('hide');
                $('#shipping_name').val(data.address_response.shipping_name);
                $('#zip').val(data.address_response.shipping_zip);
                $('#city').val(data.address_response.shipping_city);
                $('#address').val(data.address_response.shipping_state);
                $('#billing_name').val(data.address_response.billing_name);
                $('#billing_zip').val(data.address_response.billing_zip);
                $('#billing_city').val(data.address_response.billing_city);
                $('#billing_address_area').val(data.address_response.billing_state);
                $('input[id = billing_address]').val(data.address_response.billing_state);
            }

        });
    }

    $( window ).on( "load", function() {
        var user_id = <?php print user_id(); ?>;
        $.ajax({
            type: "POST",
            url: "<?= api_url('default_address_show') ?>",
            data: {
                user_id: user_id
            },
            success: function(data) {
                console.log(data.address_response);
                $('#shipping_name').val(data.address_response.shipping_name);
                $('#zip').val(data.address_response.shipping_zip);
                $('#city').val(data.address_response.shipping_city);
                $('#address').val(data.address_response.shipping_state);
                $('#billing_name').val(data.address_response.billing_name);
                $('#billing_zip').val(data.address_response.billing_zip);
                $('#billing_city').val(data.address_response.billing_city);
                $('#billing_address_area').val(data.address_response.billing_state);
                $('input[id = billing_address]').val(data.address_response.billing_state);
            }

        });
    });

    $(document).ready(function(){
    $("#checkout_address_book .address-book-wrapper .col").slice(0, 3).show();
    $("#checkout_address_book .load-more").on("click", function(e){
      e.preventDefault();
      $("#checkout_address_book .address-book-wrapper .col:hidden").slice(0, 3).slideDown();
      if($("#checkout_address_book .address-book-wrapper .col:hidden").length == 0) {
        $("#checkout_address_book .load-more").css("display", "none");
        $("#checkout_address_book .load-less").css("display", "block");
      }
      $("#checkout_address_book .load-less").on("click", function(){
            $("#checkout_address_book .load-more").css("display", "block");
            $("#checkout_address_book .load-less").css("display", "none");
            $("#checkout_address_book .address-book-wrapper .col").hide();
            $("#checkout_address_book .address-book-wrapper .col").slice(0, 3).show();

        });
    });

  })
</script>
<?php endif; ?>
