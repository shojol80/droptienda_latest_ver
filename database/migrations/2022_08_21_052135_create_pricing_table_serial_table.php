<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingTableSerialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pricing_table_serial')) {
            Schema::create('pricing_table_serial', function (Blueprint $table) {
                $table->id();
                $table->integer('table_serial')->nullable();
                $table->string('table_layouts', 255)->nullable();
                $table->longText('table_data', 255)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_table_serial');
    }
}
