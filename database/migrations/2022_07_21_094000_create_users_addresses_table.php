<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users_addresses')) {
            Schema::create('users_addresses', function (Blueprint $table) {
                $table->id();
                $table->string('address_title');
                $table->string('address_type');
                $table->string('shipping_country');
                $table->string('shipping_name');
                $table->string('shipping_city');
                $table->string('shipping_state');
                $table->string('shipping_zip');
                $table->string('billing_type');
                $table->string('billing_country');
                $table->string('billing_name');
                $table->string('billing_city');
                $table->string('billing_state');
                $table->string('billing_zip');
                $table->string('user_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_addresses');
    }
}
