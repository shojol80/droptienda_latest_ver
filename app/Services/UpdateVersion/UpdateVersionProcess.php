<?php
namespace App\Services\UpdateVersion;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ProductController;
use App\Http\Controllers\Api\V1\ApiFunctionController;


class UpdateVersionProcess
{
    public function __construct()
    {
        //constructor here 
    }

    public function index(){
        
    }

    //Update all admin dependency functionality for 1.28.0  version
    public function updated_1_28_0_admin_process(){
        $req = new Request();
        ProductController::products_transfer($req);
        ApiFunctionController::blogs_transfer($req);
        ApiFunctionController::create_vacation();
    }



    //Update all admin dependency functionality for 1.29.0  version
    public function updated_1_29_0_admin_process(){
        if(function_exists('admin_shop_menu_update')){
            admin_shop_menu_update();
        }
        if(function_exists('admin_website_menu_update')){
            admin_website_menu_update();
        }
        ApiFunctionController::drmUserNameUpdated();
    }





    //Update all admin dependency functionality for 1.29.1  version
    public function updated_1_29_1_admin_process(){
        ApiFunctionController::convert_images_to_shop();
        ApiFunctionController::update_blogs_url();
    }



    //Update all template dependency functionality for 1.29.1  version
    public function updated_1_29_1_template_process(){
        dump("From helper function for template 1.29.1 version");
    }


}
