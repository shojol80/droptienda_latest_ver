<?php
namespace App\Services\UpdateVersion;

use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use App\Services\UpdateVersion\UpdateVersionProcess;



class UpdateAdminProcess
{
    public $versionList;
    public function __construct($data)
    {
        $this->versionList = $data;
    }

    public function index(){
        $createFunctionName = str_replace('.', '_', $this->versionList);
        $createFunctionName = "updated_{$createFunctionName}_admin_process";
        $updateProcess = new UpdateVersionProcess();
        if(method_exists($updateProcess, $createFunctionName) === true){
            $updateProcess->$createFunctionName();
        }
    }



    



}
