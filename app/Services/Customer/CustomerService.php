<?php


namespace App\Services\Customer;


use App\Services\BaseService;
use Illuminate\Support\Arr;
use App\Services\DrmSyncService;
use App\Enums\SyncEvent;
use App\Enums\SyncType;

use DB;
use MicroweberPackages\User\Models\User;

class CustomerService extends BaseService
{
    public function all(array $filters = [])
    {
        $query = User::customer();

        $limit = Arr::get($filters, 'limit', 20);

        return $limit != '-1' ? $query->paginate($limit) : $query->get();
    }

    public function getById($id)
    {
        return User::customer()->find($id);
    }

    public function store(array $data)
    {
        return $this->saveCustomer($data);
    }

    public function update($id, array $data)
    {
        return $this->saveCustomer($data, $id);
    }

    public function destroy($id)
    {
        return User::customer()->where('drm_ref_id', $id)->delete();
    }

    private function saveCustomer($data, $id = null)
    {
        $product = User::firstOrNew(['drm_ref_id' => $id]);
        $product->fill($data);
        $product->save();

        return $product;
    }

    public function syncCustomerToDrm ($syncHistory)
    {
        switch ($syncHistory->sync_event) {
            case SyncEvent::CREATE:
                $customer = User::find($syncHistory->model_id);
                try{
                    if ($customer) {
                        $response = app(DrmSyncService::class)->storeCustomer([
                            'full_name' => $customer->first_name.' '.$customer->last_name,
                            'email'     => $customer->email,
                            'insert_type' => '26',
                        ]);
                        if (!empty($response['id'])) {
                            DB::table('users')->update([
                                'drm_ref_id' => $response['id'],
                            ]);
                            $syncHistory->update(['synced_at' => date('Y-m-d H:i:s')]);
                        }
                        $userGroup = array();
                        $userGroup['group_name'] = DB::table('customer_groups')->where('id',1)->pluck('group_name')->toArray()[0];
                        $userGroup['customer_id'] = $customer->id;
                        
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'http://165.22.24.129/api/droptineda-customer-group-title',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => $userGroup,
                            CURLOPT_HTTPHEADER => array(
                                'userToken: '.Config::get('microweber.userToken'),
                                'userPassToken: '.Config::get('microweber.userPassToken')
                            ),
                        ));
                        $response = curl_exec($curl);

                        curl_close($curl);
                        $response = json_decode($response, true);
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;

            case SyncEvent::UPDATE:
                $customer = User::find($syncHistory->model_id);
                try{
                    if ($customer && $customer->drm_ref_id) {
                        $response = app(DrmSyncService::class)->updateCustomer($customer->drm_ref_id, [
                            'full_name' => $customer->first_name.' '.$customer->last_name,
                            'email'     => $customer->email,
                        ]);
                        $syncHistory->update(['synced_at' => date('Y-m-d H:i:s')]);
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;

            case SyncEvent::DELETE:
                try{
                    if ( $syncHistory->drm_ref_id) {
                        app(DrmSyncService::class)->deleteCustomer($syncHistory->drm_ref_id);
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;
        }

        return $syncHistory;
    }
}
