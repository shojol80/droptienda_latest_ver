<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\UpdateVersion\UpdateAdminProcess;
use App\Services\UpdateVersion\UpdateTemplateProcess;

class VersionUpdateProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $updatedVersion;
    public $oldVersion;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($updatedVersion, $oldVersion)
    {
        $this->updatedVersion = $updatedVersion;
        $this->oldVersion = $oldVersion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->process($this->updatedVersion, $this->oldVersion);
    }

    public function process($update, $old){
        if(version_compare($update, $old) == 1){
            $executeVersion = $this->nextVersion($old);
            if(!empty($executeVersion)){
                $versionUpdated = new UpdateAdminProcess($executeVersion);
                $versionUpdated->index();
                $templateUpdated = new UpdateTemplateProcess($executeVersion);
                $templateUpdated->index();
            }
            $this->process($update, $executeVersion);
        }else if(version_compare($update, $old) == 0){
            $user_token = Config('microweber.userToken');
            $pass_token = Config('microweber.userPassToken');
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => '159.65.124.158/api/dt-channel-shop-version/update',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('version' => $this->updatedVersion),
            CURLOPT_HTTPHEADER => array(
                'userToken: '. $user_token,
                'userPassToken: '. $pass_token
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
        }
                
    }


    public function nextVersion($version){
        $version = explode('.', $version);
        if($version[2] == 9){
            $version[2] = 0;
            if($version[1] == 99){
                $version[1] = 0;
                $version[0] += 1;

            }else{
                $version[1] += 1;
            }
        }else{
            $version[2] += 1;
        }
        return implode('.', $version);
    }
}
