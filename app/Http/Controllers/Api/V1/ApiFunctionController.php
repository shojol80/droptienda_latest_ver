<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiFunctionController extends Controller
{

    public function blogs_transfer(Request $request){
        $blogs_from_content_table = DB::table('content')->where('content_type','post')->where('is_active',1)->where('is_deleted',0)->get();
        foreach($blogs_from_content_table as $single_blog){
            $blog_image_link = get_first_position_image_from_media($single_blog->id);
            DB::table('blogs')->updateOrInsert(
                ['content_id' => $single_blog->id],
                [
                    'title' => $single_blog->title,
                    'content' => $single_blog->content,
                    'link' => '{SITE_URL}'.$single_blog->url,
                    'image' => $blog_image_link,
                    'is_rss' => $single_blog->is_rss,
                    'rss_link' => $single_blog->rss_link,
                    'rss_image' => $single_blog->rss_image,
                    'created_at' => $single_blog->created_at,
                    'updated_at' => $single_blog->updated_at,
                ]
            );
        }
        dump('successfully transfer the blogs from content table to blogs table');
    }

    public function create_vacation(){
        $data = [
            "content_type"=> "page",
            "subtype"=> "static",
            "url"=> "vacation",
            "title"=> "Vacation",
            "parent"=> "0",
            "position"=> "0",
            "is_active"=> "1",
            "active_site_template"=> "default",
            "layout_file"=> "vacation.php",
            "is_home"=> "0",
            "is_pinged"=> "0",
            "is_shop"=> "0",
            "is_deleted"=> "0",
            "require_login"=> "0",
            "session_id"=> "1gFG82M8KcqrOAyLuluwfX0QvLrj3r9BMXe2jVlw",
            "updated_at"=> "2022-05-30 11:07:27",
            "created_at"=> "2022-05-30 11:07:27",
            "created_by"=> "1",
            "edited_by"=> "1",
            "posted_at"=> "2022-05-30 11:07:27",
        ];
        $status = DB::table('content')->updateOrInsert(
                                            ['url' => 'vacation'],
                                            $data);
        return response()->json(['success' => true, 'message' => 'Vacation page added successfully' ],200);
    }

    public function drmUserNameUpdated(){
        $userToken = Config('microweber.userToken');
        $passToken = Config('microweber.userPassToken');


        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://159.65.124.158/api/dt-channel-user/information',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_HTTPHEADER => array(
            'userPassToken: '.$passToken,
            'userToken: '.$userToken
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data = json_decode($response, true);
        $data = $data['data'];


        Config::set('microweber.userName', $data['name']);
        Config::save(array('microweber'));
        dump("Username Added Successfully!");
    }


    public function convert_images_to_shop(){
        $images = DB::table('media')->where('rel_type', 'content')->whereNotNull('image_id')->get()->toArray();
        if(isset($images)){
            foreach($images as $image){
                image_set_to_server($image);
            }
            dump("All images convert successfully");
        }
    }

    public function category_status(){
        $categories= DB::table('categories')
                ->where('is_deleted',0)
                ->whereNotNull('title')
                ->where('parent_id', 0)
                ->where('rel_type', 'content')
                ->where('rel_id', 2)
                ->orderBy('position', 'asc')
                ->get();
        $html=generate_shop_categories_api($categories,2,false);
        return $html;
    }

    public function update_blogs_url(){
        $blogs = DB::table('blogs')->where('link','not like','%{SITE_URL}%')->where('is_rss', 0)->get();
        if($blogs){
            foreach($blogs as $blog){
                DB::table('blogs')->updateOrInsert(
                    ['id' => $blog->id],
                    [
                        'link' => '{SITE_URL}'.$blog->link
                    ]
                );
            }
            dump("Blogs url updated successfully");
        }
    }

}
