<p>Hallo {{ username }},</p>
<br />
<p>
    soeben haben wir eine Anfrage zum Zurücksetzen deines Passworts für unseren Kundenbereich erhalten. Hier kannst du
    dein Passwort jetzt zurücksetzen:
</p>
<br />
<p>
    {{ reset_password_link }}
</p>
<br />
<p>Wenn du die Zurücksetzung nicht angefordert hast, kannst du die E-Mail ignorieren, da diese in 2 Stunden ungültig wird.</p>

