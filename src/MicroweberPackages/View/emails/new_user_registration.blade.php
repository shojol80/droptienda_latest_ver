<p>Hallo {{username}},</p>

<br />

<p>vielen Dank für deine Registrierung.</p>

<br />
<p>Bitte verifiziere jetzt deine E-Mail-Adresse, um deine Registrierung abzuschließen. </p>
<br />
{{verify_email_link}}

<br /><br /><br />

<p>Bitte verifiziere jetzt deine E-Mail-Adresse, um deine Registrierung abzuschließen. </p>
