<?php

namespace MicroweberPackages\App\Console\Commands;

use DateTime;
use Illuminate\Console\Command;
use Config;

class ShopTarrif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:tarrif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $getDayCount = Config::get('microweber.installedDate') ?? date('y-m-j');
        $installedTime = new DateTime($getDayCount);
        $currentTime = new DateTime(date('y-m-j'));
        $abs_diff = $currentTime->diff($installedTime)->format("%a");
        print_r($abs_diff);
        if($abs_diff <= 7){
            Config::set('microweber.deleteShop',$abs_diff);
            Config::save(array('microweber'));
        }elseif($abs_diff <= 11){
            Config::set('microweber.deleteShop',$abs_diff);
            Config::save(array('microweber'));
        }elseif($abs_diff <= 14){
            Config::set('microweber.deleteShop',$abs_diff);
            Config::save(array('microweber'));
        }else{
            Config::set('microweber.deleteShop',true);
            Config::save(array('microweber'));
        }
    }
}
